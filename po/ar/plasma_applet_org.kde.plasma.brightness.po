# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2024 Zayed Al-Saidi <zayed.alsaidi@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-25 00:40+0000\n"
"PO-Revision-Date: 2024-05-04 11:52+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: package/contents/ui/BrightnessItem.qml:29
#, kde-format
msgctxt "Backlight on or off"
msgid "Off"
msgstr "معطلة"

#: package/contents/ui/BrightnessItem.qml:30
#, kde-format
msgctxt "Brightness level"
msgid "Low"
msgstr "منخفض"

#: package/contents/ui/BrightnessItem.qml:31
#, kde-format
msgctxt "Brightness level"
msgid "Medium"
msgstr "متوسط"

#: package/contents/ui/BrightnessItem.qml:32
#, kde-format
msgctxt "Brightness level"
msgid "High"
msgstr "عالي"

#: package/contents/ui/BrightnessItem.qml:33
#, kde-format
msgctxt "Backlight on or off"
msgid "On"
msgstr "يعمل"

#: package/contents/ui/BrightnessItem.qml:45
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1٪"

#: package/contents/ui/KeyboardColorItem.qml:77
#, kde-format
msgid "Follow accent color"
msgstr "اتبع لون التمييز"

#: package/contents/ui/main.qml:62
#, kde-format
msgid "Brightness and Color"
msgstr "السطوع والألوان"

#: package/contents/ui/main.qml:74
#, kde-format
msgid "Screen brightness at %1%"
msgstr "سطوع الشاشة %1٪"

#: package/contents/ui/main.qml:77
#, kde-format
msgid "Keyboard brightness at %1%"
msgstr "سطوع لوحة المفاتيح %1٪"

#: package/contents/ui/main.qml:82
#, kde-format
msgctxt "Status"
msgid "Night Light suspended; middle-click to resume"
msgstr "عُلقت الإضاءة الليلية: انقر بالزر الأوسط لاستأنفها"

#: package/contents/ui/main.qml:84
#, kde-format
msgctxt "Status"
msgid "Night Light suspended"
msgstr "عُلقت الإضاءة الليلية"

#: package/contents/ui/main.qml:89
#, kde-format
msgctxt "Status"
msgid "Night Light at day color temperature"
msgstr "ضوء الليل عند حرارة لون النهار"

#: package/contents/ui/main.qml:91
#, kde-format
msgctxt "Status"
msgid "Night Light at night color temperature"
msgstr "ضوء الليل عند حرارة لون الليل"

#: package/contents/ui/main.qml:96
#, kde-format
msgctxt "Status; placeholder is a time"
msgid "Night Light in morning transition (complete by %1)"
msgstr "الضوء الليلي في الفترة الانتقالية الصباحية (اكتمل بنسبة %1)"

#: package/contents/ui/main.qml:98
#, kde-format
msgctxt "Status; placeholder is a time"
msgid "Night Light in evening transition (complete by %1)"
msgstr "الضوء الليلي في الفترة الانتقالية المسائية (اكتمل بنسبة %1)"

#: package/contents/ui/main.qml:113
#, kde-format
msgctxt "Status; placeholder is a time"
msgid "Night Light evening transition scheduled for %1"
msgstr "جدولة الانتقال المسائي للضوء الليلي عند %1"

#: package/contents/ui/main.qml:115
#, kde-format
msgctxt "Status; placeholder is a time"
msgid "Night Light morning transition scheduled for %1"
msgstr "جدولة الانتقال الصباحي للضوء الليلي عند %1"

#: package/contents/ui/main.qml:119
#, kde-format
msgid "Scroll to adjust screen brightness"
msgstr "مرر لتضبط سطوع الشاشة"

#: package/contents/ui/main.qml:122
#, kde-format
msgid "Middle-click to suspend Night Light"
msgstr "انقر بالزر الأوسط لتعليق وضع الإضاءة الليلية"

#: package/contents/ui/main.qml:210
#, kde-format
msgctxt "@action:inmenu"
msgid "Configure Night Light…"
msgstr "اضبط إضاءة الليل…"

#: package/contents/ui/NightLightItem.qml:75
#, kde-format
msgctxt "Night light status"
msgid "Suspended"
msgstr "مُعلَّق"

#: package/contents/ui/NightLightItem.qml:78
#, kde-format
msgctxt "Night light status"
msgid "Unavailable"
msgstr "غير متوفّر"

#: package/contents/ui/NightLightItem.qml:81
#, kde-format
msgctxt "Night light status"
msgid "Not enabled"
msgstr "غير ممكنة"

#: package/contents/ui/NightLightItem.qml:84
#, kde-format
msgctxt "Night light status"
msgid "Not running"
msgstr "لا تعمل"

#: package/contents/ui/NightLightItem.qml:87
#, kde-format
msgctxt "Night light status"
msgid "On"
msgstr "يعمل"

#: package/contents/ui/NightLightItem.qml:90
#, kde-format
msgctxt "Night light phase"
msgid "Morning Transition"
msgstr "انتقال الصباح"

#: package/contents/ui/NightLightItem.qml:92
#, kde-format
msgctxt "Night light phase"
msgid "Day"
msgstr "النهار"

#: package/contents/ui/NightLightItem.qml:94
#, kde-format
msgctxt "Night light phase"
msgid "Evening Transition"
msgstr "انتقال المساء"

#: package/contents/ui/NightLightItem.qml:96
#, kde-format
msgctxt "Night light phase"
msgid "Night"
msgstr "الليل"

#: package/contents/ui/NightLightItem.qml:107
#, kde-format
msgctxt "Placeholder is screen color temperature"
msgid "%1K"
msgstr "%1ك"

#: package/contents/ui/NightLightItem.qml:122
#, kde-format
msgctxt "@action:button Night Light"
msgid "Suspend"
msgstr "علّق"

#: package/contents/ui/NightLightItem.qml:146
#, kde-format
msgid "Configure…"
msgstr "اضبط…"

#: package/contents/ui/NightLightItem.qml:146
#, kde-format
msgid "Enable and Configure…"
msgstr "التمكين والضبط…"

#: package/contents/ui/NightLightItem.qml:172
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day complete by:"
msgstr "يكتمل الانتقال إلى النهار في:"

#: package/contents/ui/NightLightItem.qml:174
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night scheduled for:"
msgstr "الانتقال إلى الليل مجدول عند:"

#: package/contents/ui/NightLightItem.qml:176
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night complete by:"
msgstr "يكتمل الانتقال إلى الليل في:"

#: package/contents/ui/NightLightItem.qml:178
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day scheduled for:"
msgstr "الانتقال إلى النهار مجدول عند:"

#: package/contents/ui/PopupDialog.qml:66
#, kde-format
msgid "Display Brightness"
msgstr "سطوع العرض"

#: package/contents/ui/PopupDialog.qml:97
#, kde-format
msgid "Keyboard Brightness"
msgstr "سطوع لوحة المفاتيح"

#: package/contents/ui/PopupDialog.qml:130
#, kde-format
msgid "Keyboard Color"
msgstr "لون لوحة المفاتيح"

#: package/contents/ui/PopupDialog.qml:140
#, kde-format
msgid "Night Light"
msgstr "الإضاءة الليلية"

#~ msgctxt "Status"
#~ msgid "Night Light off"
#~ msgstr "الإضاءة الليلية معطلة"

#~ msgctxt "Night light status"
#~ msgid "Off"
#~ msgstr "معطلة"

#~ msgctxt "Status; placeholder is a temperature"
#~ msgid "Night Light at %1K"
#~ msgstr "إضاءة الليل عند %1ك"
